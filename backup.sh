#! /bin/bash -f

# must be root to ensure no access issues
if [[ `whoami` != root ]]; then
	echo "Please run this script using root."
	exit
fi

declare -i make_image=0

while [[ $# -gt 0 ]]; do
	case "$1" in 
		-f|--from)
			if [ ! -z "$2" ]; then
				disc_to_backup="$2"
			else
				echo "Please give a name for your backed up disc"
				exit
			fi
			shift
			shift
			;;
		-t|--to)
			if [ ! -z "$2" ]; then
				backup_to=$2
			else
				echo "Need a destination"
				exit
			fi
			shift
			shift
			;;
		-n|--name)
			if [ ! -z "$2" ]; then
				name="$2"
				echo $name
			else
				echo "Please give a name for your backed up disc"
				exit
			fi
			shift
			shift
			;;
		--iso)
			make_image=1;
			shift
			;;
		*)
			break
			;;
	esac
done

if [ -z "$backup_to" ]; then
	backup_to=$HOME/DISC_BACKUP/
fi

if [[ -z "$name" ]]; then
	name="Untitled_Bkup";
fi

if [[ -z "$disc_to_backup" ]]; then
	# make sure a disc is mounted
	disc_to_backup="`lsblk /dev/sr0 | tail -1 | awk '{out=""; for(i=7;i<=NF;i++){out=out$i" "}; print substr(out, 1, length(out)-1)}'`";
	if [[ ! -d "$disc_to_backup" ]]; then
		echo "$disc_to_backup"
		echo "Disc was not mounted."
		exit
	fi
fi

# do some name validation
newname="${name//[= |]/_}";
if [[ -d $backup_to"$newname" ]]; then

	# count directories with same name (plus a hyphen and some number)
	regex="^$newname(?:[\-]\d+)?$"
	declare -i count=`ls "$backup_to" | grep -P "$regex" | wc -l`;
	
	# set $newname to that name incremented by one
	next=$(( $count + 1 ));
	newname=$newname"-"$next
fi
newDir=$backup_to$newname
echo "$newDir"
# create a new directory with the name of $newname
mkdir "$newDir"
chmod 777 "$newDir"

# run backup
if [[ $make_image -ne 0 ]]; then
	info=$(isoinfo -d -i /dev/cdrom | grep -i -E 'block size|volume size' | grep -oP '\d+$')
	block_size=$(echo $info | awk -F' ' '{print $1}')
	volume_size=$(echo $info | awk -F' ' '{print $2}')
	dd if=/dev/cdrom of=$newDir/$newname.iso bs=$block_size count=$volume_size >> "$newDir/__$newname.filelist.log"
else
	rsync -rav --out-format="%t %b %'''b %f" "$disc_to_backup/" $newDir >> "$newDir/__$newname.filelist.log"
	echo `tail -2 "$newDir/__$newname.filelist.log"`
fi