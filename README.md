# Backup Your CDs and DVDs

It's getting to that time, quickly... 

I created this script to help me log my CDs and DVDs as I back them up before readers go extinct.

Contents

* [How To](#how-to)
* [Easy Version](#making-it-even-easier)

What it does

* Creates a new folder per disc
* Names the folder based on user input
  * Uses unique names
* Unless specified,
  * uses the CD ROM drive by default
  * uses the basename of "Untitled_Bkup" by default
* Backs up the disc
  * RSYNC's the data from src to dest.
    * Logs all files per disc for analytics
  * DD's the data from src to dest. \*.iso image.
    * Logs errors for analytics

## How to

```bash
	cd /path/to/backup.sh

	# ./backup.sh
	# Required:    -t | --to
	# Optional:    -f | --from
	# Recommended: -n | --name

	# Directly copying all files/folders on the disc

	./backup.sh --to /some/path --from /someother/path --name disc_name

	./backup.sh   -t /some/path     -f /someother/path     -n disc_name

	./backup.sh   -t /some/path     -n disc_name

	./backup.sh   -t /some/path


	# Ripping an ISO image of the disc... same thing plus `--iso`

	./backup.sh --to /some/path --from /someother/path --name disc_name --iso

	./backup.sh   -t /some/path     -f /someother/path     -n disc_name --iso
```

The script will automatically choose your CD drive if you don't set a `--from` or `-f` flag.

That's it!

## Making it even easier

I don't like worrying about spaces or special characters...

`save` is a header script for the other helper scripts to shorten the process but requires one little change inside the `save` file before you can run it...

Line 2: `backup_to="/path/to/backup/drive/";`

> Make sure to include the forward slash at the end

Directly copy the files/folders from the disc

```bash
	cd /path/to/backup.sh

	./copy disc name

	./copy disc-name

	./copy disc_name
```

Rip an ISO image of the disc

```bash
	cd /path/to/backup.sh

	./rip disc name

	./rip disc-name

	./rip disc_name
```